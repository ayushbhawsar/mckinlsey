const express = require('express');
const bodyParser = require('body-parser');
const userRouter = require('./routes/userRouter');
const app = express();
const port = process.env.PORT || 3000;
// Connecting to the database

// setting body parser middleware 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// API routes
app.use('/api/user', userRouter);

// Running the server
app.listen(port, () => {
    console.log(`http://localhost:${port}`)
})