
# mckinlsey
## Setup Instructions
1. Make sure node is installed.
2. clone the repository
3. goto directory
4. npm install
5. npm start

## API Endpoints
 1. POST `localhost:3000/api/user/login`
	 - Request Body:`{email:'',password''}`
	 - Response in case of valid email password combination :`{user:'',token:''}`
	 - valid credentials:`{"email":"user1@abc.com",
"password":"12345"
}`

1. POST `localhost:3000/api/user/message`
	 - add header `token` with value of token from the first api
	 - Request Body:`{
    "to": "",
    "message":"" 
}`
	 - Response in case message is sent :`Message sent`

2. GET `localhost:3000/api/user/message`
   - add header `token` with value of token from the first api
   - response `{userid1~userid2:[{time:'',message:'',from:''}]}`

 
