const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const password = '123789';
//ENCRYPT THE USER PASSWORD
exports.encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

//DECRIPT THE USER PASSWORD
exports.decrypt = function (text) {
    var decipher = crypto.createDecipher(algorithm, password);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}