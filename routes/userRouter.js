const express = require('express');
const { decrypt, encrypt } = require('../utils/cryptography');
const data = require('../data');

const userRouter = express.Router();
userRouter.route('/login').post((req, res) => {
    let body = req.body;
    if (!body.email || !body.password) {
        res.status(400).send('bad request');
    }
    if (!data.loginData[body.email]) {
        res.status(404).send('User not found');
    }
    if (data.loginData[body.email] && data.loginData[body.email].password !== body.password) {
        res.status(401).send('Invalid user credentials');
    }
    const user = data.loginData[body.email];
    const token = encrypt(`${req.body.email}~${new Date()}`);
    res.status(201).json({ user: user.name, token });
});
userRouter.route('/message').post((req, res) => {
    try {
        decrypt(req.headers.token)
    } catch (ex) {
        res.status(403).send('invalid token')
    }
    const user = data.loginData[decrypt(req.headers.token).split('~')[0]];
    if (!data.messageData[`${user.id}~${req.body.to}`] && !data.messageData[`${user.id}~${req.body.to}`]) {
        data.messageData[`${user.id}~${req.body.to}`] = [];
        data.messageData[`${user.id}~${req.body.to}`].push({
            time: (new Date()).toISOString(),
            message: req.body.message,
            from: user.id
        });
    }
    if (data.messageData[`${user.id}~${req.body.to}`]) {
        data.messageData[`${user.id}~${req.body.to}`].push({
            time: (new Date()).toISOString(),
            message: req.body.message,
            from: user.id
        });

    }
    if (data.messageData[`${req.body.to}~${user.id}`]) {
        data.messageData[`${user.id}~${req.body.to}`].push({
            time: (new Date()).toISOString(),
            message: req.body.message,
            from: user.id
        });
    }
    res.status(201).send('Message Sent');

}).get((req, res) => {
    try {
        decrypt(req.headers.token)
    } catch (ex) {
        res.status(403).send('invalid token')
    }
    const user = data.loginData[decrypt(req.headers.token).split('~')[0]];
    console.log(user.id);
    const keys = Object.keys(data.messageData);
    const response = {};
    for (let key of keys) {
        if (key.includes(user.id)) {
            response[key] = data.messageData[key];
        }
    }
    res.status(200).json(response);

});

module.exports = userRouter;